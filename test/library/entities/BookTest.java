package library.entities;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import library.entities.IBook.BookState;

class BookTest {

	IBook book, bookOnLoan, bookDamaged;
	String title = "Title";
    String author = "Author";
    String callNumber = "CallNumber";
    int id = 1;

    IBook.BookState state = IBook.BookState.AVAILABLE;	

	@BeforeEach
	void setUp() throws Exception {
		book = new Book(author, title, callNumber, id);
		bookOnLoan = new Book (author, title, callNumber, id, BookState.ON_LOAN);
		bookDamaged = new Book(author, title, callNumber, id, BookState.DAMAGED);
	}

	@AfterEach
	void tearDown() throws Exception {
		
	}

	@Test
	void testIsAvailableTrue() {
		//Arrange
		boolean expected = true;
		//Act
		boolean actual = book.isAvailable();
		//Assert
		assertEquals(expected,actual);
	}
	
	@Test
	void testIsAvailableFalseWhenON_LOAN() {
		//Arrange
		boolean expected = false;
		//Act
		boolean actual = bookOnLoan.isAvailable();
		//Assert
		assertEquals(expected,actual);
	}
	
	@Test
	void testIsAvailableFalseWhenDAMAGED() {
		//Arrange
		boolean expected = false;
		//Act
		boolean actual = bookDamaged.isAvailable();
		//Assert
		assertEquals(expected,actual);
	}

}
